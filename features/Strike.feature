Feature:
ScoreBoard showing the results of our strike in a bowling game

Scenario Outline:  Calculate Game Score for Bowling Game
    Given I play a 10 rounds Bowling game
    When I bowl a game <game>
    Then the game result will be <result>
    Examples:
    | game                         | result |
    | X 72 00 00 00 00 00 00 00 00 | 19     |
    | X X X 46 00 00 00 00 00 00   | 84     |